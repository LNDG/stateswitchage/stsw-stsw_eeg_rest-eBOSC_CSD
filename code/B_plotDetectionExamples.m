% Rhythm detection example

% 181220 | written by JQK

% The data are from an experiment where subjects rested with their eyes
% closed (EC) or eyes open (EO). This leads to dominant alpha rhythms.

% Data has been preprocessed and cleaned.
% Preprocessing included segmenting the continuous recording for cleaning
% purposes, followed by a temporal reconcatenation of matrices. This may
% lead to some sharp discontinuous edges, but should have occured fairly
% infrequently so that I wouldn't worry about it right now,

% The structure looks as follows:
% results.detectedAlpha | binary matrix: channels*time - indication of
% present (1) or absent (0) rhythmicity in the 8-12 Hz range as indicated
% by BOSC; channels are approximately frontal to posterior
% results.origData | channels*time original unfiltered data (not exclusively alpha)

% load data

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/C_eBOSC_CSD/B_data/B_eBOSCout_EO/2261_MatrixDetected_v1_EC.mat')

% plot quick overview of measures

figure; 
subplot(2,2,1); imagesc(results.detectedAlpha); title('Rhythm mask'); xlabel('Time (samples)'); ylabel('Channels')
subplot(2,2,2); imagesc(zscore(results.origData,[],2)); title('Original data'); xlabel('Time (samples)'); ylabel('Channels')
subplot(2,2,3); imagesc(results.detectedAlpha(:,1:500).*results.origData(:,1:500)); title('Rhythm-masked data, exemplary Segment'); xlabel('Time (samples)'); ylabel('Channels')
subplot(2,2,4); imagesc(results.origData(:,1:500)); title('Original data, exemplary Segment'); xlabel('Time (samples)'); ylabel('Channels')

% plot quick run through short segments

figure
for indStart = 1:500:60000
    subplot(1,2,1); imagesc(results.detectedAlpha(:,indStart:indStart+500).*results.origData(:,indStart:indStart+500)); title('Rhythmic signal');
    subplot(1,2,2); imagesc((1-results.detectedAlpha(:,indStart:indStart+500)).*results.origData(:,indStart:indStart+500)); title('Arrhythmic signal');
    pause(.5)
end
